KML Project
===========
Ca sert à rien, mais voici venir le KML Project en C ! (www.kmlproject.com)

Ceci est donc un générateur de Kamoulox en lignes de commandes, utile non
seulement pour votre divertissement personnel, mais aussi pour générer des
données de tests pour des programmes/sites web/autres.

    Usage: kamoulox [kamounom|kamouscuse|kamousulte|ipsum] [-nX] --help pour l'aide.
    Options:
    -nX       : Le nombre X de phrases générées (de 1 à 100), par défaut 1.
    --help    : Ce message.
    Types de phrases:
    kamoulox  : (Sans argument) Je tabasse un porte-avion et j'interviewe un cassoulet.
    kamounom  : Jean-Christophe Petit-Suisse
    kamouscuse: Impossible, j'ai laissé mon ostréiculteur avec une friteuse chez Roger.
    kamousulte: Espèce de sale sosie de pot de mayonnaise du sud.
    ipsum     : Lorem ipsum.

Installation
------------
Pour installer le Kamoulox, utilisez le script d'install fourni (requiert les
droits administrateur).

    ./install

Très basique, il sera normalement portable (je n'ai pas testé en fait). Si non,
vous pouvez au choix l'utiliser sans install, ou me contacter.

Les dépendances suivantes sont nécessaires :
* Korn Shell : `sudo apt-get install ksh` ou `sudo yum install ksh`
* LibXML2 : `sudo apt-get install libxml2-dev` ou `sudo yum install
  libxml2-devel`

Les exemples ci-dessus sont pour les gestionnaires de paquet apt-get et yum,
mais ça fonctionne peut-être aussi sur d'autres gestionnaires. Sinon, il reste
la possibilité de l'installation manuelle :)


Déjà disponibles
----------------
### Kamoulox
Générateur de Kamoulox simple. Utile dans toutes les situations de conversation.

    $> kamoulox -n5
    Je fais pédaler une truelle à moteur et j'épluche un boomerang.
    Je percute l'Incroyable Hulk et j'arrache un raton-laveur.
    Je décongèle Gilbert Montagné et je roule sur un Bisounours.
    Casserole de ratatouille et pelleteuse en grève.
    J'embrasse une barrière et je chatouille un poussin en faisant le poirier.

### Kamounom
Générateur de noms rigolos. Idéal pour remplir une base de données de test.

    $> kamoulox kamounom -n5
    Jean-Jacques Suppositoire
    Tony Grenouille
    Pauline Barraque-à-frites
    Marcel Touillette
    Mohammed Crevette

### Kamouscuse
Générateur d'excuses crédibles. Pratique pour décliner une réunion pro.

    $> kamoulox kamouscuse -n5
    Désolé, il y a un frigo qui essaye de me broyer avec un concombre alcoolique.
    Je peux pas, il y a un zébu qui essaye de me faire sécher avec un sumo par ordre alphabétique.
    Impossible, il y a un sombrero qui essaye de me manger pour toiletter mon bidet.
    Impossible, une terrine forestière veut me décorer chez un rideau de douche en peluche.
    Désolé, je dois aller manger ma boîte aux lettres fluorescente sur un cochon mauve.

### Kamousulte
Générateur d'insultes pas très sympas. Pour quand vous parlez politique.

    $> kamoulox kamousulte -n5
    Saleté de sosie de rateau.
    Vilain imitateur de rhinocéros en carton.
    Bouffeur de cow-boys.
    Immonde arracheur de cocottes-minute à moustaches.
    Espèce de stagiaire en pot de mayonnaise.

### Kamoumail
Générateur d'adresse email, très utile pour remplir une base de données. Un
peu comme le Kamounom, mais en pas pareil.

    $> kamoulox kamoumail -n5
    marc.antoine@assiette.com
    jean.jacques@tournevis.gov
    christophe@pigeon.voyageur.org
    ingrid@tiramisu.com
    paul@ornithologue.net

### Kamou Ipsum
Lorem Ipsum version Kamoulox, reprenant les générateurs ci-dessus. Très pratique 
lorsque vous faites du web et que vous avez surexploité le Bacon Ipsum.

    $> kamoulox ipsum -n2
    Espèce de oncle de Justin Bieber. J'insulte une cuvette et je me fais
    greffer un pompier en faisant le poirier. Désolé, je suis poursuivi 
    par un épouvantail avec un lapin en céramique. Je déballe un escalator
    de Noël et j'agrafe un pissenlit. Désolé, il y a un réacteur nucléaire
    qui essaye de me aspirer sur un castor alcoolique. Je peux pas, il y a
    un plat à gratin qui essaye de me brûler chez une corbeille à papier 
    en promotion.
    Espèce de ignoble destructeur de épouvantails. J'illumine un moteur de
    4L à la cantine et je m'installe sur un cochon. J'achète une valise 
    avec des tongs et j'aspire une cafetière. Espèce de fan de Nelson 
    Monfort. Je peux pas, je dois broyer ma tasse à café sur une 
    andouillette extraterrestre. Désolé, il faut que j'aille voir une 
    sauterelle chez un sombrero industriel.

Petits scripts à la con
-------
### MOTD

Le script *kmlmotd* vous permet de changer le message of the day sous Unix,
c'est à dire le message qui s'affiche lorsque vous vous logguez. Le message
affiché par le *kmlmotd* change tous les jours :)

### Termoulox

Termoulox vous permet d'afficher votre Kamoulox de manière relativement...
multicolore dans votre terminal. Ou d'autres textes (voir les modes
ci-dessous).

Arrêtez le termoulox en appuyant sur n'importe quelle touche (ou CTRL-C).
Sinon au bout d'un moment il s'arrête tout seul (mais c'est long).

> INFO : Si vous êtes épileptiques, vous ne devriez peut-être pas l'utiliser.

Modes :
* *Mode Kamoulox* (`./termoulox` ou `./termoulox kamoulox`) : Affiche une
  phrase générée avec le KML Project.

* *Mode Balec* (`./termoulox balec`) : Pour que votre terminal ressemble au
  clip de la chanson "On s'en bat les cou*lles" de Mr.Yéyé.

* *Mode Text* (`./termoulox n'importe quel texte`) : Affiche n'importe quel
  texte à la place du kamoulox ou du balec

Options :
*  `-a`: Affiche le texte demandé (via un mode ou directement) en ASCII art.
  Nécessite l'installation du package "figlet" (`apt-get install figlet`,
  `yum install figlet`).

Usage

```
$> ./termoulox -h
Usage: termoulox [options] text|mode (-h for help)

Affiche du texte de manière très multicolore. Attention ça pique les yeux.

 options:
-a         Affiche en ASCII art (à combiner avec un mode ou du texte)
	   Nécessite le package "figlet"

 modes:
kamoulox   (ou sans option) Génère une phrase avec le KML Project et l'affiche
balec      Permet d'exprimer son manque d'intérêt. Inspiré du clip On s'en bat
           les couilles de Mr. Yéyé

 autres arguments:
Tout argument qui n'est pas l'un de ceux ci-dessus sera affiché en tant que
texte dans le Termoulox.

Appuyez sur n'importe quelle touche pour arrêter le Termoulox
Plus d'infos dans le README.md, et ici : https://github.com/Lex2xS/kml-c
```

Par exemple :
```
$> termoulox                  # Affiche une phrase de Kamoulox
$> termoulox kamoulox         # La même chose
$> termoulox -a		      # Affiche une phrase de Kamoulox en ASCII art
$> termoulox balec	      # Affiche un truc pas très poli
$> termoulox -a balec	      # Le même en ASCII art
$> termoulox salut les gros   # Affiche "salut les gros"
$> termoulox "salut les gros" # La même chose
$> termoulox -a salut         # Affiche "salut" en ASCII art
$> termoulox salut -a	      # Affiche "salut -a" (faut mettre l'option avant)
```

>Prérequis
>
>* Le KML doit être installé sur le système, ou le binaire `kamoulox` présent
>  dans le répertoire d'exécution du termoulox. En gros, le script cherche à
>  exécuter `kamoulox` ou `./kamoulox`.
>
>* Si vous ne comptez pas utiliser le mode par défaut (kamoulox), ignorez le
>  point ci-dessus, car cela n'empêche pas les modes text et balec de
>  fonctionner

### Ajouter une commande Kamoulox dans Emacs

Vous pouvez suivre les instructions suivantes pour intégrer le KML project
dans l'éditeur Emacs et générer une phrase aléatoire avec cette commande :

    M-x kamoulox

Déjà, il faut que le KML Project soit installé sur le système (et oui :p).

Ensuite, dans un fichier de configuration .emacs dans votre dossier home, 
mettre un des codes Lisp suivant suivant le comportement souhaité :

    * Pour afficher une phrase aléatoire dans le bandeau d'options en bas:

    (defun kamoulox ()
        (interactive)
        (shell-command "kamoulox"))

    * Pour afficher une phrase aléatoire dans le buffer courant (le fichier
      que vous êtes en train d'éditer:

    (defun kamoulox (num)
        (interactive "p")
        (let ((c 0))
            (while (< c num)
                (call-process "kamoulox" nil (current-buffer) nil)
                (setf c (+ c 1)))))

Merci à Pierre pour le tuyau :D

### Résolution d'erreurs

#### Erreur libxml2
```
/bin/sh: xml2-config: command not found
srcs/kml.c:6:27: fatal error: libxml/parser.h: No such file or directory
```
Cette erreur provient du fait qu'il manque les paquets de développement
de libxml2. Pour résoudre :

* Debian/Ubuntu et ses amis : `sudo apt-get install libxml2-dev`

* CentOS/RHEL et ses amis : `sudo yum install libxml2-devel`

